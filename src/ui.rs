use nannou::{prelude::*, ui::prelude::*};

struct Elem<T> {
    id: widget::Id,
    value: T,
}

impl<T> Elem<T> {
    pub fn new(id: widget::Id, value: T) -> Self {
        Self { id, value }
    }
}

pub struct Ui {
    ui: nannou::ui::Ui,

    feed_rate: Elem<f32>,
    kill_rate: Elem<f32>,
    clear: Elem<bool>,
    random_seed: Elem<f32>,
}

impl Ui {
    pub fn new(mut ui: nannou::ui::Ui) -> Self {
        let feed_rate = Elem::new(ui.generate_widget_id(), 0.0545);
        let kill_rate = Elem::new(ui.generate_widget_id(), 0.062);
        let clear = Elem::new(ui.generate_widget_id(), false);
        let random_seed = Elem::new(ui.generate_widget_id(), 0.);

        Self {
            ui,
            feed_rate,
            kill_rate,
            clear,
            random_seed,
        }
    }

    pub fn update(&mut self) -> bool {
        let ui = &mut self.ui.set_widgets();

        fn slider(val: f32, min: f32, max: f32) -> widget::Slider<'static, f32> {
            widget::Slider::new(val, min, max)
                .w_h(200.0, 30.0)
                .label_font_size(15)
                .rgb(0.3, 0.3, 0.3)
                .label_rgb(1.0, 1.0, 1.0)
                .border(0.0)
        }

        for value in slider(self.feed_rate.value, 0.01, 0.07)
            .top_left_with_margin(20.0)
            .label("Feed Rate")
            .set(self.feed_rate.id, ui)
        {
            self.feed_rate.value = value;
        }

        for value in slider(self.kill_rate.value, 0.01, 0.07)
            .down(10.0)
            .label("Kill Rate")
            .set(self.kill_rate.id, ui)
        {
            self.kill_rate.value = value;
        }

        self.clear.value = false;
        for _ in widget::Button::new()
            .down(10.0)
            .w_h(200.0, 60.0)
            .label("Clear")
            .label_font_size(15)
            .rgb(0.3, 0.3, 0.3)
            .label_rgb(1.0, 1.0, 1.0)
            .border(0.0)
            .set(self.clear.id, ui)
        {
            self.clear.value = true;
        }

        for _ in widget::Button::new()
            .down(10.0)
            .w_h(200.0, 60.0)
            .label("Random")
            .label_font_size(15)
            .rgb(0.3, 0.3, 0.3)
            .label_rgb(1.0, 1.0, 1.0)
            .border(0.0)
            .set(self.random_seed.id, ui)
        {
            self.random_seed.value = random_f32();
        }

        match ui.global_input().current.widget_capturing_mouse {
            None => false,
            Some(widget_id) if widget_id == ui.window => false,
            Some(_) => true,
        }
    }

    pub fn draw(&self, app: &App, frame: &Frame) {
        self.ui.draw_to_frame(app, frame).unwrap();
    }

    pub fn feed_rate(&self) -> f32 {
        self.feed_rate.value
    }

    pub fn kill_rate(&self) -> f32 {
        self.kill_rate.value
    }

    pub fn clear(&self) -> bool {
        self.clear.value
    }

    pub fn random_seed(&self) -> f32 {
        self.random_seed.value
    }
}
