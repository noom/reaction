use nannou::{prelude::*, state::mouse::ButtonPosition, wgpu};

use shaderc;

use glsl_layout::{self as glsl, AsStd140};

use nalgebra_glm;

mod consts;
mod sim;
mod ui;

use consts::*;
use sim::*;
use ui::Ui;

// The vertex type that we will use to represent a point on our triangle.
#[repr(C)]
#[derive(Clone, Copy)]
struct Vertex {
    position: [f32; 2],
}

// The vertices that make up the rectangle to which the image will be drawn.
const VERTICES: [Vertex; 4] = [
    Vertex {
        position: [-1.0, -1.0],
    },
    Vertex {
        position: [-1.0, 1.0],
    },
    Vertex {
        position: [1.0, -1.0],
    },
    Vertex {
        position: [1.0, 1.0],
    },
];

impl wgpu::VertexDescriptor for Vertex {
    const STRIDE: wgpu::BufferAddress = std::mem::size_of::<Vertex>() as u64;
    const ATTRIBUTES: &'static [wgpu::VertexAttributeDescriptor] =
        &[wgpu::VertexAttributeDescriptor {
            format: wgpu::VertexFormat::Float2,
            offset: 0,
            shader_location: 0,
        }];
}

#[derive(Copy, Clone, AsStd140)]
pub struct FragUniforms {
    time: glsl::float,
    seed: glsl::float,
}

impl FragUniforms {
    pub fn new(time: f32, seed: f32) -> Self {
        Self { time, seed }
    }
}

#[repr(C)]
#[derive(Copy, Clone, AsStd140)]
pub struct CompUniforms {
    delta: glsl::float,
    feed_rate: glsl::float,
    kill_rate: glsl::float,
    mouse_i: glsl::int,
    mouse_down: glsl::boolean,
    clear: glsl::boolean,
}

type UUniforms = <CompUniforms as glsl::AsStd140>::Std140;

impl CompUniforms {
    pub fn new(
        delta: f32,
        feed_rate: f32,
        kill_rate: f32,
        mouse_i: i32,
        mouse_down: bool,
        clear: bool,
    ) -> Self {
        Self {
            delta,
            feed_rate,
            kill_rate,
            mouse_i,
            mouse_down: mouse_down.into(),
            clear: clear.into(),
        }
    }
}

fn main() {
    nannou::app(model).update(update).view(view).run();
}

fn model(app: &App) -> Model {
    let w_id = app.new_window().build().unwrap();
    let window = app.window(w_id).unwrap();
    window.set_fullscreen(false);
    let device = window.swap_chain_device();
    let format = Frame::TEXTURE_FORMAT;
    let msaa_samples = window.msaa_samples();

    // Create the UI.
    let ui = Ui::new(app.new_ui().build().unwrap());

    let vert_shader_src = include_str!("../shaders/reaction.vert");
    let frag_shader_src = include_str!("../shaders/reaction.frag");
    let comp_shader_src = include_str!("../shaders/reaction.comp");

    // Compile shaders
    let (vert_artifact, frag_artifact, comp_artifact) = {
        let mut compiler = shaderc::Compiler::new().unwrap();
        (
            compiler
                .compile_into_spirv(
                    vert_shader_src,
                    shaderc::ShaderKind::Vertex,
                    "shader.glsl",
                    "main",
                    None,
                )
                .unwrap(),
            compiler
                .compile_into_spirv(
                    frag_shader_src,
                    shaderc::ShaderKind::Fragment,
                    "shader.glsl",
                    "main",
                    None,
                )
                .unwrap(),
            compiler
                .compile_into_spirv(
                    comp_shader_src,
                    shaderc::ShaderKind::Compute,
                    "shader.glsl",
                    "main",
                    None,
                )
                .unwrap(),
        )
    };

    let vs = vert_artifact.as_binary_u8();
    let vs_mod = wgpu::shader_from_spirv_bytes(&device, vs);
    let fs = frag_artifact.as_binary_u8();
    let fs_mod = wgpu::shader_from_spirv_bytes(&device, fs);
    let cs = comp_artifact.as_binary_u8();
    let cs_mod = wgpu::shader_from_spirv_bytes(&device, cs);

    // Initialize the texture
    let texture = wgpu::TextureBuilder::new()
        .size([GRID_WIDTH as u32, GRID_HEIGHT as u32])
        .format(wgpu::TextureFormat::Rg32Float)
        .usage(wgpu::TextureUsage::COPY_DST | wgpu::TextureUsage::SAMPLED)
        .build(window.swap_chain_device());
    let texture_view = texture.view().build();

    // Create the sampler for sampling from the source texture.
    let sampler = wgpu::SamplerBuilder::new().build(device);

    // Create the buffer that will store time
    let frag_uniforms = FragUniforms::new(0., 0.);
    let frag_uniforms_buffer = device
        .create_buffer_mapped(1, wgpu::BufferUsage::UNIFORM | wgpu::BufferUsage::COPY_DST)
        .fill_from_slice(&[frag_uniforms]);

    let bind_group_layout = wgpu::BindGroupLayoutBuilder::new()
        .uniform_buffer(wgpu::ShaderStage::FRAGMENT, false)
        .sampled_texture(
            wgpu::ShaderStage::FRAGMENT,
            false,
            wgpu::TextureViewDimension::D2,
        )
        .sampler(wgpu::ShaderStage::FRAGMENT)
        .build(device);
    let bind_group = wgpu::BindGroupBuilder::new()
        .buffer::<FragUniforms>(&frag_uniforms_buffer, 0..1)
        .texture_view(&texture_view)
        .sampler(&sampler)
        .build(device, &bind_group_layout);
    let pipeline_layout = create_pipeline_layout(device, &bind_group_layout);
    let render_pipeline = create_render_pipeline(
        device,
        &pipeline_layout,
        &vs_mod,
        &fs_mod,
        format,
        msaa_samples,
    );

    // Create the vertex buffer.
    let vertex_buffer = device
        .create_buffer_mapped(VERTICES.len(), wgpu::BufferUsage::VERTEX)
        .fill_from_slice(&VERTICES[..]);

    let grid_buffer_size =
        (GRID_WIDTH * GRID_HEIGHT * std::mem::size_of::<[f32; 2]>()) as wgpu::BufferAddress;
    let mut grid = Grid::new();
    // grid.init_seeds();
    grid.init_seeds_center();
    let grid_buffer = device
        .create_buffer_mapped(
            GRID_WIDTH * GRID_HEIGHT,
            wgpu::BufferUsage::STORAGE | wgpu::BufferUsage::COPY_DST | wgpu::BufferUsage::COPY_SRC,
        )
        .fill_from_slice(grid.0.as_slice().clone());

    // Create the buffer that will store delta.
    let uniforms = CompUniforms::new(0., 0., 0., 0, false, false);
    let uniforms_buffer = device
        .create_buffer_mapped(1, wgpu::BufferUsage::UNIFORM | wgpu::BufferUsage::COPY_DST)
        .fill_from_slice(&[uniforms]);

    let compute_bind_group_layout = wgpu::BindGroupLayoutBuilder::new()
        .storage_buffer(wgpu::ShaderStage::COMPUTE, false, false)
        .uniform_buffer(wgpu::ShaderStage::COMPUTE, false)
        .build(device);
    let compute_bind_group = wgpu::BindGroupBuilder::new()
        .buffer_bytes(&grid_buffer, 0..grid_buffer_size)
        .buffer::<CompUniforms>(&uniforms_buffer, 0..1)
        .build(device, &compute_bind_group_layout);

    let compute_pipeline_layout = create_pipeline_layout(device, &compute_bind_group_layout);
    let compute_pipeline = {
        let compute_stage = wgpu::ProgrammableStageDescriptor {
            module: &cs_mod,
            entry_point: "main",
        };
        let desc = wgpu::ComputePipelineDescriptor {
            layout: &compute_pipeline_layout,
            compute_stage,
        };
        device.create_compute_pipeline(&desc)
    };

    Model::new(
        ui,
        bind_group,
        render_pipeline,
        vertex_buffer,
        frag_uniforms_buffer,
        compute_bind_group,
        compute_pipeline,
        grid_buffer,
        texture,
        uniforms_buffer,
        1.,
    )
}

fn update(app: &App, model: &mut Model, update: Update) {
    let window = app.main_window();
    let device = window.swap_chain_device();

    let handle_mouse = !model.ui.update();

    let time = update.since_start.as_millis() as f32;

    let delta = {
        let delta = (time - model.last_time) / 20.;
        let max = 0.8;
        // let max = 1.;
        let delta = if delta > max || delta <= 0. {
            max
        } else {
            delta
        };
        delta
    };

    // Update last time
    model.last_time = time;

    let mouse_i: i32 = {
        let (x, y): (f32, f32) = app.mouse.position().into();
        let (w, h) = window.rect().w_h();
        map_range(-(y - h / 2.), 0., h, 0, GRID_HEIGHT as i32) * GRID_WIDTH as i32
            + map_range(x + w / 2., 0., w, 0, GRID_WIDTH as i32)
    };
    let mouse_down: bool = if let ButtonPosition::Down(_) = app.mouse.buttons.left() {
        true && handle_mouse
    } else {
        false
    };

    // Update delta
    let uniforms = CompUniforms::new(
        delta,
        model.ui.feed_rate(),
        model.ui.kill_rate(),
        mouse_i,
        mouse_down,
        model.ui.clear(),
    )
    .std140();
    let uniforms_size = std::mem::size_of::<UUniforms>() as wgpu::BufferAddress;
    let new_uniform_buffer = device
        .create_buffer_mapped(1, wgpu::BufferUsage::COPY_SRC)
        .fill_from_slice(&[uniforms]);

    if model.iteration % 50 == 0 {
        println!(
            "iteration: {}; fps: {}, since_last: {:?}",
            model.iteration,
            app.fps(),
            update.since_last
        );
    }

    let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor::default());
    encoder.copy_buffer_to_buffer(
        &new_uniform_buffer,
        0,
        &model.comp_uniforms_buffer,
        0,
        uniforms_size,
    );

    {
        let mut cpass = encoder.begin_compute_pass();
        cpass.set_pipeline(&model.compute_pipeline);
        cpass.set_bind_group(0, &model.compute_bind_group, &[]);
        let work_groups_x = (GRID_WIDTH / WORK_GROUP_SIDE) as u32;
        let work_groups_y = (GRID_HEIGHT / WORK_GROUP_SIDE) as u32;
        for _ in 0..STEP_PER_ITER {
            cpass.dispatch(work_groups_x, work_groups_y, 1);

            model.iteration += 1;
        }
    }

    // Submit the compute pass to the device's queue.
    window
        .swap_chain_queue()
        .lock()
        .unwrap()
        .submit(&[encoder.finish()]);
}

fn view(app: &App, model: &Model, frame: Frame) {
    {
        let window = app.main_window();
        let device = window.swap_chain_device();

        let mut encoder = frame.command_encoder();

        let uniforms = FragUniforms::new(app.time, model.ui.random_seed()).std140();
        let uniforms_size = std::mem::size_of::<FragUniforms>() as wgpu::BufferAddress;
        let new_uniform_buffer = device
            .create_buffer_mapped(1, wgpu::BufferUsage::COPY_SRC)
            .fill_from_slice(&[uniforms]);
        encoder.copy_buffer_to_buffer(
            &new_uniform_buffer,
            0,
            &model.frag_uniform_buffer,
            0,
            uniforms_size,
        );

        let buffer_copy_view = model.texture.default_buffer_copy_view(&model.grid_buffer);
        let texture_copy_view = model.texture.default_copy_view();
        let extent = model.texture.extent();
        encoder.copy_buffer_to_texture(buffer_copy_view, texture_copy_view, extent);

        let mut render_pass = wgpu::RenderPassBuilder::new()
            .color_attachment(frame.texture_view(), |color| {
                // color
                color.clear_color(wgpu::Color {
                    r: 0.23,
                    g: 0.,
                    b: 0.38,
                    a: 1.,
                })
            })
            .begin(&mut encoder);
        render_pass.set_bind_group(0, &model.bind_group, &[]);
        render_pass.set_pipeline(&model.render_pipeline);
        render_pass.set_vertex_buffers(0, &[(&model.vertex_buffer, 0)]);
        let vertex_range = 0..VERTICES.len() as u32;
        let instance_range = 0..1;
        render_pass.draw(vertex_range, instance_range);
    }

    model.ui.draw(app, &frame);
}

fn create_pipeline_layout(
    device: &wgpu::Device,
    bind_group_layout: &wgpu::BindGroupLayout,
) -> wgpu::PipelineLayout {
    let desc = wgpu::PipelineLayoutDescriptor {
        bind_group_layouts: &[&bind_group_layout],
    };
    device.create_pipeline_layout(&desc)
}

fn create_render_pipeline(
    device: &wgpu::Device,
    layout: &wgpu::PipelineLayout,
    vs_mod: &wgpu::ShaderModule,
    fs_mod: &wgpu::ShaderModule,
    dst_format: wgpu::TextureFormat,
    sample_count: u32,
) -> wgpu::RenderPipeline {
    wgpu::RenderPipelineBuilder::from_layout(layout, vs_mod)
        .fragment_shader(fs_mod)
        .color_format(dst_format)
        .add_vertex_buffer::<Vertex>()
        .sample_count(sample_count)
        .primitive_topology(wgpu::PrimitiveTopology::TriangleStrip)
        .build(device)
}
