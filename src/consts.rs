pub const GRID_WIDTH: usize = 800;
pub const GRID_HEIGHT: usize = 608;
pub const WORK_GROUP_SIDE: usize = 32; // WARNING: bigger value can crash the gpu

// pub const STEP_PER_ITER: usize = 15; // default
pub const STEP_PER_ITER: usize = 15;
// pub const STEP_PER_ITER: usize = 50;

pub const MIN_SEEDS: usize = 1;
pub const MAX_SEEDS: usize = 20;
pub const SEED_MIN_SIZE: usize = 14;
pub const SEED_MAX_SIZE: usize = 1;
pub const SEED_PROB_PER_CELL: f64 = 1.;

// pub const SEED_MIN_WIDTH: usize = 2;
// pub const SEED_MIN_HEIGHT: usize = 2;
// pub const SEED_MAX_WIDTH: usize = 50;
// pub const SEED_MAX_HEIGHT: usize = 50;
