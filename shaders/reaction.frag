#version 450

const float PI = 3.141592653589793;
const float PHI = 1.61803398874989484820459;
const float SEED = 347457016375973.;

layout(location = 0) in vec2 tex_coords;
layout(location = 0) out vec4 f_color;

layout(set = 0, binding = 0) uniform uniforms {
    float t;
    float seed;
};

layout(set = 0, binding = 1) uniform texture2D tex;
layout(set = 0, binding = 2) uniform sampler tex_sampler;

float random (vec2 st) {
    return fract(sin(dot(st.xy, vec2(12.9898,78.233))) * 43758.5453123);
}

float gold_noise(vec2 xy, float seed) {
    return fract(tan(distance(xy*PHI, xy)*seed)*xy.x);
}

float to_lin(in float s) {
    float linear;
    if (s <= 0.04045) linear = s / 12.92;
    else linear = pow((s + 0.055) / 1.055, 2.4);
    return linear;
}
vec3 to_lin(in vec3 color) {
    return vec3(to_lin(color.r), to_lin(color.g), to_lin(color.b));
}

float hue2rgb(float f1, float f2, float hue) {
    if (hue < 0.0)
        hue += 1.0;
    else if (hue > 1.0)
        hue -= 1.0;
    float res;
    if ((6.0 * hue) < 1.0)
        res = f1 + (f2 - f1) * 6.0 * hue;
    else if ((2.0 * hue) < 1.0)
        res = f2;
    else if ((3.0 * hue) < 2.0)
        res = f1 + (f2 - f1) * ((2.0 / 3.0) - hue) * 6.0;
    else
        res = f1;
    return res;
}
vec3 hsl2rgb(vec3 hsl) {
    vec3 rgb;
    if (hsl.y == 0.0) {
        rgb = vec3(hsl.z); // Luminance
    } else {
        float f2;
        if (hsl.z < 0.5)
            f2 = hsl.z * (1.0 + hsl.y);
        else
            f2 = hsl.z + hsl.y - hsl.y * hsl.z;
        float f1 = 2.0 * hsl.z - f2;
        rgb.r = hue2rgb(f1, f2, hsl.x + (1.0/3.0));
        rgb.g = hue2rgb(f1, f2, hsl.x);
        rgb.b = hue2rgb(f1, f2, hsl.x - (1.0/3.0));
    }
    return rgb;
}
vec3 hsl2rgb(float h, float s, float l) {
    return hsl2rgb(vec3(h, s, l));
}

vec3 hsb2rgb( in vec3 c ){
    vec3 rgb = clamp(abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),
                             6.0)-3.0)-1.0,
                     0.0,
                     1.0 );
    rgb = rgb*rgb*(3.0-2.0*rgb);
    return c.z * mix( vec3(1.0), rgb, c.y);
}

float map(float value, float min1, float max1, float min2, float max2) {
  return min2 + (value - min1) * (max2 - min2) / (max1 - min1);
}

vec3 ran_color(float x) {
    vec3 color = hsb2rgb(vec3(random(vec2(x)), 0.8, 0.5));

    return color;
}

vec3 ran_gradiant(float seed, int count, float off, float range, float transition_length, float value) {
    vec3 color = vec3(0.);

    for (int i = 0; i < count; i++) {
        vec3 color_step = ran_color(seed + i);

        float first = step(1., i);
        float pct = smoothstep(
            (range / count * i - transition_length + off) * first,
            (range / count * i + transition_length + off) * first,
            value
        );

        color = mix(color, color_step, pct);
    }

    return color;
}

void main() {
    /* float a = texture(sampler2D(tex, tex_sampler), tex_coords).r; */
    vec2 tex = texture(sampler2D(tex, tex_sampler), tex_coords).rg;
    float a = tex.r;
    float b = tex.g;

    float range = 0.36;

    // Nice purple/yellow/blue
    /* vec4 bc = vec4(hsl2rgb(map(b, 0., 0.36, 0., 1.), 0.7, 0.4), b * 3.7); */
    /* vec4 bc = vec4(hsl2rgb(map(b, 0.5, 1., 0., 1.), 0.7, 0.4), b * 3.7); */
    /* vec4 bc = vec4(hsl2rgb(map(b, 0., 0.3, 0., 1.), 0.7, 0.4), b * 3.7); */

    /* vec3 color = ran_gradiant(seed, 4, 0., 1., 0.085, fract(b + (t / 4.))); */

    /* float alpha = smoothstep(0., 0.1, b); */
    /* float alpha = map(b, 0., range, 0., 1.); */
    float alpha = 1.;
    /* float alpha = b; */

    /* vec3 color = vec3(smoothstep(0., 0.2, b) * step(b, 0.2)); */
    vec3 color = vec3(smoothstep(0.12, 0.2, b) * (1. - smoothstep(0.2, 0.27, b)));
    color *= vec3(0.4, (sin(t * 2.5) * 0.5 + 0.5) * 0.3, 0.59 * (sin(t * 2.5) * 0.5 + 0.5) + tex_coords.x * tex_coords.y);

    vec4 bc = vec4(to_lin(color), alpha);

    f_color = bc;
}
